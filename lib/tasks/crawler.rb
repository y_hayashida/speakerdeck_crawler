require 'open-uri'
require 'nokogiri'
require 'net/http'
require 'uri'
require 'json'
require 'csv'

class Tasks::Crawler

  TECHNOLOGY_URL = "https://speakerdeck.com/c/technology"
  PROGRAMMING_URL = "https://speakerdeck.com/c/programming"
  SLIDE_BASE_URL = "https://speakerdeck.com"
  PAGE_PARAM = "?page="

  def self.technology_crawling()
    csv_generate(get_slides(TECHNOLOGY_URL), "technology_slides")
  end

  def self.programming_crawling()
    csv_generate(get_slides(PROGRAMMING_URL), "programming_slides")
  end

  def self.get_slides(base_url)
    slides = []
    for page in 1..1000
      page_s = page.to_s
      slides.concat(get_slide_data(base_url + PAGE_PARAM + page_s))
      puts "#{page}ページ取得" if page % 20 == 0
      sleep(rand(2..4))
    end
    slides
  end

  def self.get_slide_data(url)
    slides = []
    retrycount = 0
    doc = nil
    while retrycount < 3 do
      begin
        doc = Nokogiri.HTML(open(url))
        break
      rescue Timeout::Error
        retrycount + 1
      rescue => e
        retrycount + 1
      end
    end
    if retrycount == 3
      puts "取得失敗 URL: #{url}"
      return slides
    end
    doc.css('.col-12.col-md-6.col-lg-4.mb-5 > a').each do |element|
      slides << [element[:title], SLIDE_BASE_URL + element[:href]]
    end
    slides
  end

  def self.csv_generate(params, name)
    bom = %w(EF BB BF).map { |e| e.hex.chr }.join.force_encoding("UTF-8")
    csv_data = CSV.generate(bom) do |csv|
      params.each do |slide|
        csv << slide
      end
    end
    File.open("./#{name}.csv", 'w') do |file|
      file.write(csv_data)
    end
  end

end
