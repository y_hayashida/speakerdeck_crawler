# SpearkerDeckの情報収集クローラー

## 概要

SpearkerDeckのスライドへのURLとタイトルをcsvデータ化するクローラー

## 使い方
- bin/rails runner Tasks::Crawler.technology_crawling

https://speakerdeck.com/c/technology のスライドを1000ページ分取得してcsvデータとして出力する

- bin/rails runner Tasks::Crawler.programming_crawling

https://speakerdeck.com/c/programming のスライドを1000ページ分取得してcsvデータとして出力する

## 備考
- 1000ページ分取得するのにかかる時間はおよそ90分程度。
- lib/tasks/crawler.rb 以外ほとんど変更していないので、無駄なgem、ファイルや設定が多い。
